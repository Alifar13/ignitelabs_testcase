package com.example.ali.ignitelab_testcase.Activities;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

public class LoginViewModel extends ViewModel {

    private LoginModel loginmodel;
    private LoginCallBacks logincallbacks;


    private Context context;





    public LoginViewModel(LoginCallBacks logincallbacks) {
        this.loginmodel = new LoginModel();
        this.logincallbacks = logincallbacks;


    }



    public LoginModel getLoginmodel() {
        return loginmodel;
    }

    public void setLoginmodel(LoginModel loginmodel) {
        this.loginmodel = loginmodel;
    }

    public LoginCallBacks getLogincallbacks() {
        return logincallbacks;
    }

    public void setLogincallbacks(LoginCallBacks logincallbacks) {
        this.logincallbacks = logincallbacks;
    }


    public TextWatcher emailTextWatcher()
    {

    return new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {


            loginmodel.setUserName(editable.toString());



        }
    };

    }



    public TextWatcher passwordTextWatcher()
    {

        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                loginmodel.setPassword(editable.toString());



            }
        };

    }


    public void LoginClick(View view)
    {

        if(loginmodel.isValid())
        {

            logincallbacks.onSuccess("success");




        }
        else
            {

                logincallbacks.onFailure("failed");

            }



    }




}
