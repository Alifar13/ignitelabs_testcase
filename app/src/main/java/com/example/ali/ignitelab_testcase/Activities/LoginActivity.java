package com.example.ali.ignitelab_testcase.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.ali.ignitelab_testcase.R;
import com.example.ali.ignitelab_testcase.databinding.LoginActivityBinding;

public class LoginActivity extends AppCompatActivity implements  LoginCallBacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

         LoginActivityBinding activityloginBinding= DataBindingUtil.setContentView(this,R.layout.login_activity);

         activityloginBinding.setViewModel(ViewModelProviders.of(this,new LoginViewModelFactory((LoginCallBacks) this)).get(LoginViewModel.class));





    }

    @Override
    public void onSuccess(String message) {

        Toast.makeText(this,"success",Toast.LENGTH_LONG).show();

        Intent i = new Intent(getApplicationContext(),MainActivity.class);

       startActivity(i);

    }

    @Override
    public void onFailure(String message) {

        Toast.makeText(this,"Failure",Toast.LENGTH_LONG).show();


    }
}
