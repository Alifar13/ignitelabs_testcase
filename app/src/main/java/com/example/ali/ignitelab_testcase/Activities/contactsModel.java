package com.example.ali.ignitelab_testcase.Activities;

public class contactsModel {

    String id;

    String Name;

    String Number;

    public contactsModel(String id, String name, String number) {
        this.id = id;
        Name = name;
        Number = number;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }
}
