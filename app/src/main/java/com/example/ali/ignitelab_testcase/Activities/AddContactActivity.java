package com.example.ali.ignitelab_testcase.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ali.ignitelab_testcase.Activities.api;
import com.example.ali.ignitelab_testcase.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddContactActivity extends AppCompatActivity {


    private api Api;

    Retrofit retrofit;

    private static final String TAG = "AddContactActivity";

    EditText edName,edNumber;

    Button btnSave;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);


        edName=findViewById(R.id.txtName);
        edNumber=findViewById(R.id.txtNumber);

        btnSave=findViewById(R.id.btnSave);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Name = edName.getText().toString().trim();
                String Number = edNumber.getText().toString().trim();
                if(!TextUtils.isEmpty(Name) && !TextUtils.isEmpty(Number)) {
                    sendPost(Name, Number);
                }
            }
        });





    }



    public void sendPost(String Name, String Number) {


        retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api = retrofit.create(api.class);



        Api.saveContact(Name, Number).enqueue(new Callback<contactsModel>() {

            @Override
            public void onResponse(Call<contactsModel> call, Response<contactsModel> response) {
                if(response.isSuccessful()) {
                    showResponse(response.body().toString());

                    Log.i(TAG ,"post submitted to API." + response.body().toString());

                    Toast.makeText(getApplicationContext(),"successfully added.",Toast.LENGTH_LONG).show();




                }
            }

            @Override
            public void onFailure(Call<contactsModel> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }



        });
    }
    public void showResponse(String response) {
        Log.e(TAG, response);

    }






}
