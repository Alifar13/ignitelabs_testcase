package com.example.ali.ignitelab_testcase.Activities;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public class LoginModel {

    @Nullable
    String UserName,password;


    public LoginModel() {
    }

    public LoginModel(String userName, String password) {
        UserName = userName;
        this.password = password;
    }

    @Nullable
    public String getUserName() {
        return UserName;
    }

    public void setUserName(@Nullable String userName) {
        UserName = userName;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    public boolean isValid()
    {

    return !TextUtils.isEmpty(UserName) && !TextUtils.isEmpty(password);
    }
}
