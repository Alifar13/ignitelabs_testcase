package com.example.ali.ignitelab_testcase.Activities;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ali.ignitelab_testcase.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListActivity extends AppCompatActivity {


    ListView listView;

    api api;

    Dialog dialog;


    String edDialogName,edDialogNumber;


    private static final String TAG = "ListActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView=findViewById(R.id.listViewcontacts);

        getList();


        dialog = new Dialog(this);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {



                String selectedFromList = (listView.getItemAtPosition(i).toString());

                String[] text = selectedFromList.split(": ");


                edDialogName=text[1];

                edDialogNumber=text[2];

                showPopUp(Integer.parseInt(text[0]),edDialogName,edDialogNumber);




                Toast.makeText(getApplicationContext(),"Position:  "+selectedFromList,Toast.LENGTH_LONG).show();




            }
        });

    }



    private void getList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

         api = retrofit.create(api.class);

        Call<List<contactsModel>> call = api.getContacts();

        call.enqueue(new Callback<List<contactsModel>>() {
            @Override
            public void onResponse(Call<List<contactsModel>> call, Response<List<contactsModel>> response) {
                List<contactsModel> contactList = response.body();

                String[] contacts = new String[contactList.size()];

                for (int i = 0; i < contactList.size(); i++) {
                    contacts[i] = contactList.get(i).getId()+": "+contactList.get(i).getName()+": "+contactList.get(i).getNumber();


                }


                listView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, contacts));

            }

            @Override
            public void onFailure(Call<List<contactsModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void deleteContact(int id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(api.class);

        Call<Void> call = api.deletePost(id);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(),"Deleted!!!",Toast.LENGTH_LONG).show();


                getList();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }


    private void updateContact(int id,String name,String number) {
        contactsModel model = new contactsModel(Integer.toString(id),name,number);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(api.class);

        Call<contactsModel> call = api.patchContact(id, model);

        call.enqueue(new Callback<contactsModel>() {
            @Override
            public void onResponse(Call<contactsModel> call, Response<contactsModel> response) {

                if (!response.isSuccessful()) {

                    Log.d(TAG, "code: "+response.code());

                    return;
                }


                Toast.makeText(getApplicationContext(),"Updated!!!",Toast.LENGTH_LONG).show();

                getList();


            }

            @Override
            public void onFailure(Call<contactsModel> call, Throwable t) {

                Log.d(TAG, "onFailure: "+t.getMessage());

            }
        });
    }



    public void showPopUp(final int id,final String name,final String number)
    {

        dialog.setContentView(R.layout.popup);

        Button btnDel =  dialog.findViewById(R.id.btnDel);



        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteContact(id);

            }
        });


        Button btnUp=dialog.findViewById(R.id.btnUpdate);

        final EditText edName=dialog.findViewById(R.id.txtname);

        final EditText edNumber=dialog.findViewById(R.id.txtnumber);


        edName.setText(name);

        edNumber.setText(number);


        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                updateContact(id,edName.getText().toString(),edNumber.getText().toString());


            }
        });





        dialog.show();




    }




}
