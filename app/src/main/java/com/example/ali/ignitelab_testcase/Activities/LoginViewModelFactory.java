package com.example.ali.ignitelab_testcase.Activities;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

public class LoginViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private LoginCallBacks loginCallBacks;

    Context context;

    public LoginViewModelFactory(LoginCallBacks loginCallBacks) {
        this.loginCallBacks = loginCallBacks;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new LoginViewModel(loginCallBacks);
    }
}
