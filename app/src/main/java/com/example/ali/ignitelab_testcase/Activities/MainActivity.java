package com.example.ali.ignitelab_testcase.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ali.ignitelab_testcase.R;

public class MainActivity extends AppCompatActivity {


    Button updatebtn,adnewbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        updatebtn=findViewById(R.id.btnupdate_contact);
        adnewbtn=findViewById(R.id.btninsert_contact);



        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getBaseContext(),ListActivity.class);
                startActivity(i);

            }
        });

        adnewbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(getBaseContext(),AddContactActivity.class);
                startActivity(i);

            }
        });



    }







}
