package com.example.ali.ignitelab_testcase.Activities;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface api {

    String BASE_URL = "https://5d9ae597686ed000144d1895.mockapi.io/api/v1/";

    @GET("igniteLab")
    Call<List<contactsModel>> getContacts();

    @POST("igniteLab")
    @FormUrlEncoded

    Call<contactsModel> saveContact(@Field("Name") String Name,
                           @Field("Number") String Number
    ) ;



    @DELETE("igniteLab/{id}")
    Call<Void> deletePost(@Path("id") int id);



    @PATCH("igniteLab/{id}")
    Call<contactsModel> patchContact(@Path("id") int id, @Body contactsModel cmodel);




}
